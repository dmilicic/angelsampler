import csv, sys, os
import fnmatch

RECORDING_INTERVAL = 4000
INTERVAL = 10 # how many milliseconds of data are we gonna aggregate

class SensorData:

	def __init__(self, x, y, z):
		self.x = float(x)
		self.y = float(y)
		self.z = float(z)
		self.cnt = 1

	def add(self, x, y, z):
		self.x += float(x)
		self.y += float(y)
		self.z += float(z)
		self.cnt += 1

	def avg(self):
		avgx = self.x / self.cnt
		avgy = self.y / self.cnt
		avgz = self.z / self.cnt
		return avgx, avgy, avgz

def parse_sensor_data(data):

	output = []

	current = 0 # pointer to the current interval starting point
	last = len(data) - 1

	sensorData = None

	# knock flag
	knock = int(data[0][-1])


	for idx, row in enumerate(data):
		timestamp = int(row[0])
		knock = int(row[-1])

		x = row[1]
		y = row[2]
		z = row[3]

		if idx == last:


			current += INTERVAL

			if current > RECORDING_INTERVAL:
				break

			sensorData.add(x, y, z)
			avgx, avgy, avgz = sensorData.avg()

			tmp = [current, avgx, avgy, avgz, knock]
			output.append(tmp)
		elif timestamp < current + INTERVAL:
			if sensorData is None:
				sensorData = SensorData(x, y, z)
			else:
				sensorData.add(x, y, z)

		else:
			current += INTERVAL

			if sensorData is None:
				sensorData = SensorData(x, y, z)

			avgx, avgy, avgz = sensorData.avg()

			tmp = [current, avgx, avgy, avgz, knock]
			output.append(tmp)

			sensorData = SensorData(x, y, z)

	while len(output) < RECORDING_INTERVAL / INTERVAL:
		current += INTERVAL
		row = [current, 0, 0, 0, knock]
		output.append(row)

	return output

def parse_mic_data(data):
	output = []

	current = 0 # pointer to the current interval starting point

	amplitude = 0
	cnt = 0
	last = len(data) - 1

	# knock flag
	knock = int(data[0][-1])

	for idx, row in enumerate(data):

		timestamp = int(row[0])

		if idx == last:
			current += INTERVAL

			if current >= RECORDING_INTERVAL:
				break

			amplitude += int(row[1])
			cnt += 1

			avg_amplitude = amplitude / cnt
			tmp = [current, avg_amplitude, knock]
			output.append(tmp)

		elif timestamp < current + INTERVAL:
			amplitude += int(row[1])
			cnt += 1
		else:
			current += INTERVAL
			if amplitude == 0 or cnt == 0: # there is nothing in this interval
				tmp = [current, 0, knock]
			else:
				avg_amplitude = amplitude / cnt
				tmp = [current, avg_amplitude, knock]

			output.append(tmp)

			amplitude = int(row[1])
			cnt = 1

	while len(output) < RECORDING_INTERVAL / INTERVAL:
		current += INTERVAL
		row = [current, 0, knock]
		output.append(row)

	return output

def merge_data(acc_data, gyro_data, mic_data):

	if len(acc_data) != len(gyro_data) or len(mic_data) != len(acc_data):
		raise ValueError("Data length should match!" + " Lengths: " + str(len(acc_data)) +  " " + str(len(gyro_data)) + " " + str(len(mic_data)))

	output = []
	for idx, row_acc in enumerate(acc_data):

		row_audio = mic_data[idx]
		row_gyro = gyro_data[idx]

		timestamp = row_acc[0]
		knock = row_acc[-1]

		acc_x = row_acc[1]
		acc_y = row_acc[2]
		acc_z = row_acc[3]

		gyro_x = row_gyro[1]
		gyro_y = row_gyro[2]
		gyro_z = row_gyro[3]

		mic = row_audio[1]

		output.append([timestamp, acc_x, acc_y, acc_z, gyro_x, gyro_y, gyro_z, mic, knock])

	return output


def extract_timestamps(files, ext='*.txt'):

	result = set()

	# filter all other files that are not data files
	files = [f for f in files if fnmatch.fnmatch(f, ext)]

	for f in files:
		splitted = f.split("_")
		timestamp = splitted[2].split(".")[0]
		result.add(timestamp)

	return result

def main(absolute_path_dir):
	# get all files from the directory
	sample_directory = os.listdir(absolute_path_dir)
	# timestamp determines our sample ID
	id_list = extract_timestamps(sample_directory)

	# prepare the output directory
	samples_output = os.path.join(absolute_path_dir, "samples")
	if not os.path.exists(samples_output):
	    os.makedirs(samples_output)

	# counts how many samples are created
	samples_cnt = 0

	# count empty sample files for debugging
	empty_cnt = 0

	for timestamp in id_list:

		# this should result in a list of absolute path files with the same timestamp (same sample ID)
		files = [absolute_path_dir + f for f in sample_directory if fnmatch.fnmatch(f, "*_" + timestamp + ".txt")]

		file_acc = open(files[0], 'rt')
		file_mic = open(files[1], 'rt')
		file_gyro = open(files[2], 'rt')

		try:
			# parse from CSV
			acc_reader = list(csv.reader(file_acc))
			gyro_reader = list(csv.reader(file_gyro))
			mic_reader = list(csv.reader(file_mic))

			if len(acc_reader) == 0 or len(gyro_reader) == 0 or len(mic_reader) == 0:
				empty_cnt += 1
				print "Empty files:", files[0], files[1], files[2]
				continue

			# prepare the output file
			filename_out = os.path.join(samples_output, "sample_" + timestamp + ".csv")
			file_out = open(filename_out, "wt")

			# parse sensor/mic data to a usable format
			accelerometer = parse_sensor_data(acc_reader)
			gyro = parse_sensor_data(gyro_reader)
			mic = parse_mic_data(mic_reader)

			# merge sensor and mic data together to create a single sample
			data = merge_data(accelerometer, gyro, mic)


			samples_cnt = samples_cnt + 1

			# convert samples to CSV and write it out to samples/ folder
			writer = csv.writer(file_out)
			for row in data:
				writer.writerow( row )

		except:
			print "An exception has occured, report this to dario.milicic@bellabeat.com and also send all files that are printed here:"
			print files[0], files[1], files[2]
		finally:
		    file_acc.close()
		    file_gyro.close()
		    file_mic.close()
		    file_out.close()

	print "Empty files found:", empty_cnt
	print "Created", samples_cnt, "samples in", samples_output


if __name__ == '__main__':
	main(sys.argv[1])