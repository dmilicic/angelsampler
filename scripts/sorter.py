import sys, os, csv
import fnmatch
import parser

from os import listdir
from os.path import isfile, join

def main(samples_dir):

	# prepare the output directory
	samples_output = os.path.join(samples_dir, "sorted")
	if not os.path.exists(samples_output):
	    os.makedirs(samples_output)

	# get the list of all samples in this directory
	files = [ f for f in listdir(samples_dir) if isfile(join(samples_dir,f)) ]
	# filter non-csv files
	files = [ samples_dir + f for f in files if fnmatch.fnmatch(f, "*.csv")]

	files_len = len(files)
	cnt = 0

	for f in files:


		# extract and reverse the timestamp
		splitted_by_underscore = f.split("_")
		splitted_by_dot = splitted_by_underscore[1].split(".")
 		reversed_timestamp = splitted_by_dot[0]
 		timestamp = reversed_timestamp[::-1]

 		# prepare the output file
		sorted_sample_name = "sample_" + timestamp + ".csv"
		file_out = open(os.path.join(samples_output, sorted_sample_name), "wt")

		# read the sample
		sample_file = open(f, "rt")
		sample_file_data = sample_file.read()

		# write the sample
		file_out.write(sample_file_data)

		# release
		file_out.close()


if __name__ == '__main__':
	main(sys.argv[1])