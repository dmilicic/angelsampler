import sys, os, csv
import fnmatch
import parser


from os import listdir
from os.path import isfile, join
from random import randrange

def main(samples_dir):

	# prepare the output directory
	samples_output = os.path.join(samples_dir, "test")
	if not os.path.exists(samples_output):
	    os.makedirs(samples_output)

	# mapping folder contains a file that maps individual test samples to check if it is a KNOCK or not
	mapping_output = os.path.join(samples_dir, "map")
	if not os.path.exists(mapping_output):
	    os.makedirs(mapping_output)
	mapping_file = open(os.path.join(mapping_output, "map.txt"), "wt")

	# get the list of all samples in this directory
	files = [ f for f in listdir(samples_dir) if isfile(join(samples_dir,f)) ]
	# filter non-csv files
	files = [ samples_dir + f for f in files if fnmatch.fnmatch(f, "*.csv")]

	for f in files:

		# a random stamp that will scramble the sample name
		stamp = randrange(1, 10000)

 		# prepare the output file
		scrambled_sample_name = "sample_" + str(stamp) + ".csv"
		file_out = open(os.path.join(samples_output, scrambled_sample_name), "wt")
		# load the writer
		writer = csv.writer(file_out)

		# read the sample
		sample_file = open(f, "rt")
		sample_reader = csv.reader(sample_file)

		# write the new csv file with ommited knock column
		for row in sample_reader:
			knock = row[-1]
			writer.writerow(row[:-1])

		# map this sample to its knock or non-knock class
		mapping_file.write(scrambled_sample_name + " " + knock + "\n")

		# release
		file_out.close()


if __name__ == '__main__':
	main(sys.argv[1])