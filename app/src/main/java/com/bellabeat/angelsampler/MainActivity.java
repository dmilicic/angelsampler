package com.bellabeat.angelsampler;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import timber.log.Timber;
import timber.log.Timber.DebugTree;


public class MainActivity extends ActionBarActivity
        implements SensorEventListener, OnClickListener {

    private          SensorManager m_sensorManager;
    private volatile long          m_startTime;
    private          long          m_accEvents;
    private          long          m_gyroEvents;
    private          long          m_proxEvents;
    private          long          m_volumeEvents;

    private Handler m_handler;

    private int m_recordings;

    /**
     * see https://stackoverflow.com/questions/11071035/unregistering-sensormanager-doesnt-work
     */
    private static boolean ANDROID_BUG;

    private static final int SAMPLE_RATE = 44100;
    private volatile boolean     m_isRecording;
    private volatile boolean     m_recordingStarted;
    private          AudioRecord m_audioRecorder;

    private BufferedWriter m_accWriter;
    private BufferedWriter m_gyroWriter;
    private BufferedWriter m_proxWriter;
    private BufferedWriter m_audioWriter;

    private File m_dir; // Our directory for storing samples

    private static final String PREF_NAME         = "angelsample_prefs";
    private static final String KEY_RECORDING_CNT = "recordings_cnt";

    private static final int RECORD_INTERVAl = 4000; // Duration of the interval we are recording (in milliseconds)

    @InjectView(R.id.button_start_recording) Button      m_button;
    @InjectView(R.id.progressBar)            ProgressBar m_progress;
    @InjectView(R.id.checkBox)               CheckBox    m_checkBox;
    @InjectView(R.id.recordingNum)           TextView    m_recordingNum;
    @InjectView(R.id.delay)                  EditText    m_delay;

    private class startRecordingRunnable implements Runnable {

        @Override
        public void run() {

            // You shouldn't be able to select if a sample is a knock sample during the recording
            m_checkBox.setClickable(false);

            m_progress.setProgress(0);
            // Let user know which recording number is he on
            m_recordingNum.setText("Recording num. " + String.valueOf(m_recordings));

            // Timestamp will represent the unique ID across all samples and all devices
            long timestamp = System.currentTimeMillis();
            // Reverse the timestamp string for easier lookup
            String timestamp_reverse = new StringBuilder(String.valueOf(timestamp)).reverse().toString();

            // Initialize output files
            String accFilePath = "acc_sample_" + String.valueOf(timestamp_reverse) + ".txt";
            String gyroFilePath = "gyro_sample_" + String.valueOf(timestamp_reverse) + ".txt";
            String audioFilePath = "audio_sample_" + String.valueOf(timestamp_reverse) + ".txt";

            try {
                File accFile = new File(m_dir, accFilePath);
                File gyroFile = new File(m_dir, gyroFilePath);
                File audioFile = new File(m_dir, audioFilePath);

                m_accWriter = new BufferedWriter(new FileWriter(accFile));
                m_gyroWriter = new BufferedWriter(new FileWriter(gyroFile));
                m_audioWriter = new BufferedWriter(new FileWriter(audioFile));
                Timber.w("File path: " + accFile.getAbsolutePath());
                Timber.w("File path: " + gyroFile.getAbsolutePath());
                Timber.w("File path: " + audioFile.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }

            // Initialize sensors and start recording
            m_sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
            boolean acceleration = m_sensorManager.registerListener(MainActivity.this,
                    m_sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION),
                    SensorManager.SENSOR_DELAY_FASTEST);
            boolean gyroscope = m_sensorManager.registerListener(MainActivity.this,
                    m_sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE),
                    SensorManager.SENSOR_DELAY_FASTEST);

            Timber.w("Acc: " + String.valueOf(acceleration) + " Gyro: " + String.valueOf(gyroscope));
            // Needed sensors are not available
            if (!acceleration || !gyroscope) {
                Toast.makeText(MainActivity.this, "Error: sensors unavailable!", Toast.LENGTH_LONG).show();
                stopRecording();
                return;
            }

            ANDROID_BUG = false;

            // Start capturing microphone data
            startAudioRecording();

            Timber.d("Started recording " + String.valueOf(m_recordings) + "...");
            m_recordings++;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (BuildConfig.DEBUG) {
            Timber.plant(new DebugTree());
        }

        m_handler = new Handler(Looper.getMainLooper());

        // Initialize output directory
        File root = Environment.getExternalStorageDirectory();
        m_dir = new File(root.getAbsolutePath() + "/angelsample");
        m_dir.mkdir();

        // Check if there are already some files and update recording count if necessary
        int fileCnt = m_dir.listFiles().length;
        if (fileCnt != 0) {
            // A single sample is made out of 3 files
            m_recordings = fileCnt / 3;
            storeRecordNumber();
        }

        Timber.w("Files found: " + String.valueOf(m_dir.listFiles().length));
        m_startTime = -1;
    }

    @Override
    protected void onResume() {
        super.onResume();
        ButterKnife.inject(this);
        m_button.setOnClickListener(this);
        // Get our recording count - we will append this to our file names
        m_recordings = getRecordingNum();
    }

    @Override
    protected void onPause() {
        storeRecordNumber();
        super.onPause();
    }

    @Override
    public void onClick(View v) {

        // Disable the start recording button for now
        m_button.setOnClickListener(null);

        // How much to delay the start of the recording
        long delay = 0;
        String sDelay = m_delay.getText().toString();
        if (!TextUtils.isEmpty(sDelay)) {
            delay = Long.valueOf(m_delay.getText().toString());
        }
        delay = delay * 1000; // Convert to milliseconds

        m_recordingStarted = false;

        m_handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                // Play sound indicating recording started
                AudioManager audioManager = (AudioManager) MainActivity.this.getSystemService(Context.AUDIO_SERVICE);
                int volume = audioManager.getStreamVolume(AudioManager.STREAM_SYSTEM);
                volume = (int) ((volume * 1.0 / audioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM)) * 100);
                ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, volume);
                toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 100);

            }
        }, delay);

        m_handler.postDelayed(new startRecordingRunnable(), delay + 150);
    }


//    @OnClick(R.id.button_start_recording)
//    public void onClick() {
//
//        // How much to delay the start of the recording
//        long delay = 0;
//        String sDelay = m_delay.getText().toString();
//        if (!TextUtils.isEmpty(sDelay)) {
//            delay = Long.valueOf(m_delay.getText().toString());
//        }
//        delay = delay * 1000; // Convert to milliseconds
//
//        m_recordingStarted = false;
//
//        m_handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//                // Play sound indicating recording started
//                AudioManager audioManager = (AudioManager) MainActivity.this.getSystemService(Context.AUDIO_SERVICE);
//                int volume = audioManager.getStreamVolume(AudioManager.STREAM_SYSTEM);
//                volume = (int) ((volume * 1.0 / audioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM)) * 100);
//                ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, volume);
//                toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 100);
//
//            }
//        }, delay);
//
//        m_handler.postDelayed(new startRecordingRunnable(), delay + 150);
//    }

    private void storeRecordNumber() {
        // Store our recording count
        SharedPreferences prefs = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        Editor editor = prefs.edit();
        editor.putInt(KEY_RECORDING_CNT, m_recordings);
        editor.apply();
    }

    private int getRecordingNum() {
        SharedPreferences prefs = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return prefs.getInt(KEY_RECORDING_CNT, 0);
    }

    private void stopRecording() {

        // Re-enable the start recording button
        m_button.setOnClickListener(this);

        // Restart the timer
        m_startTime = -1;

        // Stop all sensor listeners
        ANDROID_BUG = true;
        m_sensorManager.unregisterListener(this);
        m_sensorManager = null;

        // Set progress to 100%
        m_progress.setProgress(100);

        // Enable the knock checkbox
        m_checkBox.setClickable(true);

        // Stop capturing microphone data
        stopAudioRecording();

        Timber.d("Stopped recording...");
        Timber.d("Acc events: " + String.valueOf(m_accEvents));
        Timber.d("Gyro events: " + String.valueOf(m_gyroEvents));
        Timber.d("Audio events: " + String.valueOf(m_volumeEvents));

        // reset counters
        m_accEvents = 0;
        m_gyroEvents = 0;
        m_proxEvents = 0;
        m_volumeEvents = 0;

        try {
            m_gyroWriter.close();
            m_accWriter.close();

            m_gyroWriter = null;
            m_accWriter = null;
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }

        storeRecordNumber();

        // Play sound indicating recording finished
        AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        int volume = audioManager.getStreamVolume(AudioManager.STREAM_SYSTEM);
        volume = (int) ((volume * 1.0 / audioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM)) * 100);
        ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, volume);
        toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if (ANDROID_BUG)
            return;

        // Start the timer when sensors start reporting data
        startTimer();

        float time_elapsed = System.currentTimeMillis() - m_startTime;

        switch (event.sensor.getType()) {
            case Sensor.TYPE_LINEAR_ACCELERATION:
                getAccelerometer(event);
                break;
            case Sensor.TYPE_GYROSCOPE:
                getGyroscope(event);
                break;
        }

        // set progress bar
        int progress = (int) ((time_elapsed / RECORD_INTERVAl) * 100);
        m_progress.setProgress(progress);

        // Stop after a time interval
        if (time_elapsed > RECORD_INTERVAl) {
            stopRecording();
        }

    }

    private void startAudioRecording() {
        final int minBufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
        m_audioRecorder = new AudioRecord(MediaRecorder.AudioSource.MIC, SAMPLE_RATE,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT,
                minBufferSize);

        Timber.w("Min audio buffer size: " + String.valueOf(minBufferSize));

        m_audioRecorder.startRecording();
        m_isRecording = true;

        Thread audioRecorderThread = new Thread(new Runnable() {
            @Override
            public void run() {
                short[] buffer = new short[minBufferSize];
                StringBuilder sb = new StringBuilder();

                while (m_isRecording) { // Extract data as long as we are recording

                    m_volumeEvents++;

                    int readSize = m_audioRecorder.read(buffer, 0, buffer.length);

                    if (readSize > 0) { // Write buffer data to file

                        for (int i = 0; i < readSize; i++) {
                            final short amplitude = buffer[i];

                            // Recording has stopped in another thread, so stop here as well
                            if (m_startTime == -1) break;

                            long timestamp = System.currentTimeMillis() - m_startTime;

                            if (timestamp > RECORD_INTERVAl) // timestamp is out of our recording range
                                break;

                            sb.setLength(0); // Reset the string builder
                            sb.append(String.valueOf(timestamp))
                                    .append(",")
                                    .append(String.valueOf(amplitude))
                                    .append(",");

                            appendKnockFlag(sb);
                            sb.append("\n");

                            try {
                                m_audioWriter.write(sb.toString());
                            } catch (IOException e) {
                                Timber.w("Recording stopped so we shouldn't write to files anymore.");
                                e.printStackTrace();
                            }
                        }
                    }
                }

                // Close the writer
                try {
                    m_audioWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                // Clear memory
                if (m_audioRecorder != null) {
                    m_audioRecorder.release();
                    m_audioRecorder = null;
                }
            }
        });

        audioRecorderThread.start();
    }

    private void stopAudioRecording() {
        m_isRecording = false;
    }

    public void appendKnockFlag(StringBuilder sb) {
        // Adds 1 if KNOCK should occur
        if (m_checkBox.isChecked()) {
            sb.append("1");
        } else {
            sb.append("0");
        }
    }

    private void startTimer() {
        if (m_recordingStarted == false) {
            m_startTime = System.currentTimeMillis();
            m_recordingStarted = true;
        }
    }

    private void getGyroscope(SensorEvent event) {
        m_gyroEvents++;

        // Axis of the rotation sample, not normalized yet.
        float axisX = event.values[0];
        float axisY = event.values[1];
        float axisZ = event.values[2];

        String output = prepareOutput(event.values);
        try {
            m_gyroWriter.write(output);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getAccelerometer(SensorEvent event) {
        m_accEvents++;
        float[] values = event.values;
        // Movement
        float x = values[0];
        float y = values[1];
        float z = values[2];

        String output = prepareOutput(event.values);
        try {
            m_accWriter.write(output);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String prepareOutput(float[] values) {
        float x = values[0];
        float y = values[1];
        float z = values[2];

        long timestamp = System.currentTimeMillis() - m_startTime;

        StringBuilder sb = new StringBuilder();
        sb.append(String.valueOf(timestamp))
                .append(",")
                .append(String.valueOf(x))
                .append(",")
                .append(String.valueOf(y))
                .append(",")
                .append(String.valueOf(z))
                .append(",");

        appendKnockFlag(sb);
        sb.append("\n");

        return sb.toString();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }
}
